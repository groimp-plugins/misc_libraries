# a list of required jars for groimp

These libraries can be added to a maven build by adding the repository :

```xml
<repositories>
  <repository>
    <id>remote-gitlab-maven-libs</id>
    <name>lib</name>
    <url>https://gitlab.com/groimp-plugins/misc_libraries/-/blob/298d3f84b1d5254a2a481406894f0d86ce801f20/</url>
  </repository>
</repositories>
```

then like normal maven dependencies following the path groupId/artifactId/VERSION/artifactId-VERSION.jar  (e.g):

```xml
<dependency>
<groupId>raskob.geometry</groupId>
  <artifactId>raskob</artifactId>
  <version>1.0</version>
</dependency>
```
